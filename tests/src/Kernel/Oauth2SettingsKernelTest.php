<?php

namespace Drupal\Tests\oauth2\Kernel;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Test OAuth2 settings.
 *
 * @group oauth2
 */
class Oauth2SettingsKernelTest extends Oauth2KernelTestBase {

  /**
   * Tests public/private keys set in the OAuth2 settings config.
   */
  public function testKeys() {
    $settings = $this->config(static::SETTINGS_CONFIG);
    $public_key_path = $settings->get(static::PUBLIC_KEY_PATH);
    $this->assertFileExists($public_key_path);
    $this->assertTrue(is_readable($public_key_path), new FormattableMarkup('The @file is readable.', ['@file' => $public_key_path]));
    $private_key_path = $settings->get(static::PRIVATE_KEY_PATH);
    $this->assertFileExists($private_key_path);
    $this->assertTrue(is_readable($private_key_path), new FormattableMarkup('The @file is readable.', ['@file' => $private_key_path]));
  }

}
