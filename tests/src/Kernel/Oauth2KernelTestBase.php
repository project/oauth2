<?php

namespace Drupal\Tests\oauth2\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Defines a base class for OAuth2 kernel testing.
 */
abstract class Oauth2KernelTestBase extends KernelTestBase {

  /**
   * The OAuth2 settings config.
   */
  const SETTINGS_CONFIG = 'oauth2.settings';

  /**
   * The public key.
   */
  const PUBLIC_KEY_PATH = 'public_key_path';

  /**
   * The private key.
   */
  const PRIVATE_KEY_PATH = 'private_key_path';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'oauth2',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $keys = $this->generateKeys();
    $this->writeKeys($keys);

    $this->config(self::SETTINGS_CONFIG)
      ->set(self::PUBLIC_KEY_PATH, $this->getKeyPath('public'))
      ->set(self::PRIVATE_KEY_PATH, $this->getKeyPath('private'))
      ->save();
  }

  /**
   * Generates public/private keys.
   *
   * @return array
   *   Returns array with public and private keys.
   */
  protected function generateKeys(): array {
    $cert_config = [
      'digest_alg' => 'sha512',
      'private_key_bits' => 4096,
      'private_key_type' => OPENSSL_KEYTYPE_RSA,
    ];
    // Create the public key.
    $key = openssl_pkey_new($cert_config);
    // Extract the private key from $key to $private_key.
    openssl_pkey_export($key, $private_key);
    // Extract the public key from $key to $public_key.
    $public_key = openssl_pkey_get_details($key);

    return [
      self::PUBLIC_KEY_PATH => $public_key['key'],
      self::PRIVATE_KEY_PATH => $private_key,
    ];
  }

  /**
   * Writes the public/private keys inside of keys dir.
   *
   * @param array $keys
   *   The generated keys.
   */
  protected function writeKeys(array $keys): void {
    $file_permission = 0600;
    mkdir("{$this->siteDirectory}/keys", $file_permission);
    $public_key_path = $this->getKeyPath('public');
    $private_key_path = $this->getKeyPath('private');
    file_put_contents($public_key_path, $keys[self::PUBLIC_KEY_PATH]);
    file_put_contents($private_key_path, $keys[self::PRIVATE_KEY_PATH]);
    chmod($public_key_path, $file_permission);
    chmod($private_key_path, $file_permission);
  }

  /**
   * Get the public/private key path.
   *
   * @param string $type
   *   The type of key: public or private.
   *
   * @return string
   *   Returns the path to the public/private key.
   */
  protected function getKeyPath(string $type): string {
    return "{$this->siteDirectory}/keys/{$type}.key";
  }

}
