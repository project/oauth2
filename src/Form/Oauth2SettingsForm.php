<?php

namespace Drupal\oauth2\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The OAuth2 settings form.
 *
 * @package Drupal\oauth2\Form
 */
class Oauth2SettingsForm extends ConfigFormBase {

  /**
   * The OAuth2 settings config.
   */
  const SETTINGS_CONFIG = 'oauth2.settings';

  /**
   * The public key.
   */
  const PUBLIC_KEY_PATH = 'public_key_path';

  /**
   * The private key.
   */
  const PRIVATE_KEY_PATH = 'private_key_path';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oauth2_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::SETTINGS_CONFIG];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS_CONFIG);

    $form[self::PUBLIC_KEY_PATH] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public key path'),
      '#description' => $this->t('The path to the public key file.'),
      '#default_value' => $config->get(self::PUBLIC_KEY_PATH),
      '#element_validate' => ['::validateFile'],
      '#required' => TRUE,
    ];
    $form[self::PRIVATE_KEY_PATH] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private key path'),
      '#description' => $this->t('The path to the private key file.'),
      '#default_value' => $config->get(self::PRIVATE_KEY_PATH),
      '#element_validate' => ['::validateFile'],
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(self::SETTINGS_CONFIG)
      ->set(self::PUBLIC_KEY_PATH, $form_state->getValue(self::PUBLIC_KEY_PATH))
      ->set(self::PRIVATE_KEY_PATH, $form_state->getValue(self::PRIVATE_KEY_PATH))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Validates if the file exists and if it's readable.
   *
   * @param array $element
   *   The element being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public function validateFile(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if (!empty($element['#value'])) {
      $path = $element['#value'];
      // Does the file exist?
      if (!@file_exists($path)) {
        $form_state->setError($element, $this->t('The %field file does not exist.', ['%field' => $element['#title']]));
      }
      // Is the file readable?
      if (!@is_readable($path)) {
        $form_state->setError($element, $this->t('The %field file at the specified location is not readable.', ['%field' => $element['#title']]));
      }
    }
  }

}
