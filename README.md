### Requirements
- [OpenSSL](https://www.openssl.org/)

### Installation

1. Create a `keys` directory outside document root.
```
mkdir keys
```
2. Generate public/private key inside the `keys` directory.
```
openssl genrsa -out private.key 4096
openssl rsa -in private.key -pubout > public.key
```
3. Set 600 or 660 file permission recursive on the `keys` directory:
```
chmod -R 600 keys
```
4. Set paths to the generated keys in: `/admin/config/oauth2/oauth-settings`.
